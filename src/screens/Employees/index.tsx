import React, { useEffect, useState } from 'react';
import { FlatList, Platform, View } from 'react-native';

import { LoadingIndicator, SafeAreaView } from '../../components';
import { Employee } from '../../components/Employee';
import { useListPersons } from '../../hooks/persons';
import { IEmployee } from '../../types/employee';

import styles from './styles';
import { Text } from '../../components/Field/Text';

export function EmployeesScreen() {
  const [list, setList] = useState<IEmployee[]>([]);
  const { data, error, isLoading, refetch, fetchNextPage, isFetchingNextPage } =
    useListPersons();

  useEffect(() => {
    if (data) {
      setList(data.pages.flatMap(d => d.data));
    }
  }, [data]);

  useEffect(() => {
    if (error) {
      console.error(error);
    }
  }, [error]);

  return (
    <SafeAreaView>
      <FlatList<IEmployee>
        refreshing={isLoading}
        keyExtractor={item => item.email.toString()}
        onRefresh={refetch}
        data={list}
        renderItem={({ item }) => {
          if(Platform.OS === 'android') {
            return (
              <View style={{
                  borderRadius: 20,
                  width: '100%', 
                  height: 100, 
                  justifyContent: 'space-between', 
                  flexDirection: 'row', 
                  alignItems: 'center', 
                  borderColor: 'black'
                }}>
                <Employee
                  item={{
                    email: item.email,
                    firstname: item.firstname,
                    lastname: item.lastname,
                    phone: item.phone,
                    website: item.website,
                  }}
                />
              </View>
            )
          } else {
            return (
              <Employee
                item={{
                  email: item.email,
                  firstname: item.firstname,
                  lastname: item.lastname,
                  phone: item.phone,
                  website: item.website,
                }}
              />
            )
          }
        }}
        onEndReachedThreshold={100}
        onEndReached={() => fetchNextPage()}
        ListFooterComponent={
          isLoading || isFetchingNextPage ? <LoadingIndicator /> : <></>
        }
        ItemSeparatorComponent={() => <View style={styles.separator} />}
      />
    </SafeAreaView>
  );
}
