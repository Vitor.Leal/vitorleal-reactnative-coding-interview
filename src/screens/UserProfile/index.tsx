import React from 'react';
import { SafeAreaView } from '../../components';
import { Text } from '../../components/Field/Text';

export function UserProfile() {

  return (
    <SafeAreaView>
      <Text>
        Welcome to your profile!
      </Text>
    </SafeAreaView>
  );
}
