import React from 'react';

import { createStackNavigator } from '@react-navigation/stack';
import { EmployeeDetailScreen, EmployeesScreen } from '../screens';
import { ScreenParamsList } from './paramsList';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { UserProfile } from '../screens/UserProfile';

const Stack = createStackNavigator<ScreenParamsList>();
const Drawer = createDrawerNavigator();

export function MainNavigator() {

  return (
    <Stack.Navigator
      screenOptions={{
        headerTintColor: '#6C3ECD',
        headerTitleAlign: 'center',
      }}>
      <Stack.Screen component={EmployeesScreen} name="Employees" />
      <Stack.Screen component={EmployeeDetailScreen} name="EmployeeDetail" />
    </Stack.Navigator>
  );
}

export function DrawerNavigator() {

   return (
    <Drawer.Navigator initialRouteName="Home">
      <Drawer.Screen name="Stack" component={MainNavigator} />
      <Drawer.Screen name="Home" component={EmployeesScreen} />
      <Drawer.Screen name="User profile" component={UserProfile} />
    </Drawer.Navigator>
   ) 
}
