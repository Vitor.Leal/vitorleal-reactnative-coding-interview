export interface IEmployee {
  id?: number;
  firstname: string;
  lastname: string;
  email: string;
  website: string;
  phone: string;
}
